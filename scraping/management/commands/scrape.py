from django.core.management.base import BaseCommand
from urllib.request import urlopen
from bs4 import BeautifulSoup
from scraping.models import Position
import json

class Command(BaseCommand):
    help = "collect positions"

    # define logic of command
    def handle(self, *args, **options):
        # collect html
        html = urlopen('https://jobs.lever.co/opencare')

        # convert to soup
        soup = BeautifulSoup(html, 'html.parser')

        # grab all postings
        postings = soup.find_all("div", class_="posting")

        for post in postings:
            url = post.find('a', class_='posting-btn-submit')['href']
            title = post.find('h5').text
            # location = post.find('span', class_='sort-by-location'.text)

            # check if url in db
            try:
                # save in db
                Position.objects.create(
                    url=url,
                    title=title,
                    # location=location
                )
                print('%s added' % (title,))
            except:
                print('%s already exists' % (title,))

        self.stdout.write('completed')
